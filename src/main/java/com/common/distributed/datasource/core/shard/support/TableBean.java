/**
 * 
 */
package com.common.distributed.datasource.core.shard.support;

import java.io.Serializable;

/**
 * @author liubing1
 * 
 */
public class TableBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2662430180662615369L;

	private String name;

	private String prefixname;

	public TableBean() {

	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the prefixname
	 */
	public String getPrefixname() {
		return prefixname;
	}

	/**
	 * @param prefixname
	 *            the prefixname to set
	 */
	public void setPrefixname(String prefixname) {
		this.prefixname = prefixname;
	}
}
