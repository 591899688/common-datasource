/**
 * 
 */
package com.common.distributed.datasource.exception;

/**
 * @author liubing1
 * 数据源错误异常
 */
public class CommonDatasourceException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 702035873879198253L;
	
	public CommonDatasourceException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public CommonDatasourceException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public CommonDatasourceException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public CommonDatasourceException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public CommonDatasourceException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}
}
