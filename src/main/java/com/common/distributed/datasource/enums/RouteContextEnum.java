/**
 * 
 */
package com.common.distributed.datasource.enums;

/**
 * @author liubing1
 * 
 */
public enum RouteContextEnum {

	tablename, // 表名

	tablefieldname;// 字段名
}
