/**
 * 
 */
package test.common.distributed.datasource.ibatis.dao;

import java.util.Map;

import test.common.distributed.datasource.ibatis.model.DemoModel;
import test.common.distributed.datasource.ibatis.model.DemoParam;


/**
 * @author liubing
 *
 */
public interface IDemoDao {
	/**
	 * 插入
	 * @param demoModel
	 */
	public void insert(DemoParam demoParam) throws Exception;
	/**
	 * 按照主键删除
	 * @param i
	 */
	public void delete(Map<String, Object> params) throws Exception;
	/**
	 * 修改
	 * @param demoModel
	 */
	public void update(DemoParam demoParam) throws Exception;
	/**
	 * 按照主键查询
	 * @param id
	 * @return
	 */
	public DemoModel findById(Map<String, Object> params) throws Exception;
}
